#!/usr/bin/env python

"""
Test python API to qdo
"""

import sys
import os
import qdo

def test_ntasks(q, **kwargs):
    """
    Check the number of tasks in each state.
    If a state isn't listed in kwargs, assume 0.
    """
    ntasks = q.tasks(summary=True)
    for state in ['Waiting', 'Pending', 'Running', 'Succeeded', 'Failed']:
        n = kwargs[state] if (state in kwargs) else 0            
        failmsg = "{}={} instead of {}".format(state, ntasks[state], n)
        assert ntasks[state] == n, failmsg
    

#----- create, delete, and connect
print "-- qdo.create, q.delete, qdo.connect"
qname = 'TestBlatFoo'
q = qdo.create(qname)
q.delete()

try:
    q = qdo.connect(qname)
    print "Oops!  Delete didn't work!"
    sys.exit(1)
except ValueError:
    #- ok, the delete worked, so put the queue back now
    q = qdo.create(qname)

#----- q.add
print "-- q.add"
n = 3
for i in range(n):
    cmd = 'echo Hello {}'.format(i)
    q.add(cmd)
test_ntasks(q, Pending=n)

#----- q.add_multiple
print "-- q.add_multiple"
cmds = ['oops {}'.format(i) for i in range(n)]
q.add_multiple(cmds)
test_ntasks(q, Pending=2*n)

#----- q.loadfile
print "-- q.loadfile"
cmdfile = qname+'.txt'
fx = open(cmdfile, 'w')
for i in range(n):
    print >> fx, 'echo Goodbye {}'.format(i)
fx.close()
q.loadfile(cmdfile)
test_ntasks(q, Pending=3*n)
q.loadfile(cmdfile, priority=1)
test_ntasks(q, Pending=4*n)

#----- q.do
print "-- q.do"
q.do(timeout=1, quiet=True)
test_ntasks(q, Succeeded=3*n, Failed=n)

#----- q.tasks
print "-- q.tasks"
tasks = q.tasks()
assert len(tasks) == 4*n
assert type(tasks[0]) == qdo.Task

#- filter by state
print "-- q.tasks while filtering by state"
tasks = q.tasks(state=qdo.Task.SUCCEEDED)
assert len(tasks) == 3*n
tasks = q.tasks(state=qdo.Task.FAILED)
assert len(tasks) == n

#- get a single task
print "-- q.tasks : get single task"
t = q.tasks(id=tasks[0].id)
assert t.id == tasks[0].id

#- Try to get an invalid task
print "-- q.tasks : try to get invalid task id"
try:
    t = q.tasks(id='blatfoo!')
except ValueError:
    print "Correctly got ValueError for invalid task id"
    t = 'Yep This Worked'
assert t == 'Yep This Worked', "Whoops, asking for invalid task ID didn't raise a ValueError"

#----- q.retry
print "-- q.retry"
q.retry()
test_ntasks(q, Succeeded=3*n, Pending=n)

#----- q.rerun
print "-- q.rerun : get single task"
q.rerun()
test_ntasks(q, Pending=4*n)

#----- q.get
print "-- q.get"
t = q.get()
assert t.state == qdo.Task.RUNNING
test_ntasks(q, Pending=4*n-1, Running=1)

#----- q.pop
print "-- q.pop"
t = q.pop()
assert t.state == qdo.Task.RUNNING
test_ntasks(q, Pending=4*n-2, Running=2)

#----- q.recover
print "-- q.recover"
q.recover()
test_ntasks(q, Pending=4*n)

#----- q.pause / q.resume / q.state
print "-- q.pause, q.resume, q.state"
assert q.state == qdo.Queue.ACTIVE
q.pause()
assert q.state == qdo.Queue.PAUSED
assert q.get() is None
q.resume()
assert q.state == qdo.Queue.ACTIVE

#----- Task.run
print "-- t.run"
t = q.get()
test_ntasks(q, Pending=4*n-1, Running=1)
t.run()
test_ntasks(q, Pending=4*n-1, Succeeded=1)

#----- qdo.qlist
print "-- qdo.qlist"
assert q.name in [x.name for x in qdo.qlist()]

#----- Task manipulation
print "-- task state manipulation"
t = q.get()
t.set_state(qdo.Task.WAITING)
assert q.tasks(t.id).state == qdo.Task.WAITING

message = 'epic fail'
t.set_state(qdo.Task.FAILED, err=1, message=message)
tx = q.tasks(t.id)
assert tx.state == qdo.Task.FAILED
assert tx.err == 1
assert tx.message == message

#----- running with a script
print "-- q.do with a script"
q.delete()
q = qdo.create(qname)
q.add_multiple(['hello {}'.format(i) for i in range(n)])
test_ntasks(q, Pending=n)
q.do(quiet=True, script='echo', timeout=1)
test_ntasks(q, Succeeded=n)

#----- tasks as lists
print "-- tasks as lists not strings"
q.delete()
q = qdo.create(qname)
q.add_multiple([ [i,i+1] for i in range(n)])
test_ntasks(q, Pending=n)
q.do(quiet=True, script='echo counting {} {}', timeout=1)
test_ntasks(q, Succeeded=n)

#----- tasks as dictionaries
print "-- tasks as dictionaries not strings"
q.delete()
q = qdo.create(qname)
q.add_multiple([ dict(a=i, b=i+1) for i in range(n)])
test_ntasks(q, Pending=n)
q.do(quiet=True, script='echo counting again {a} {b}', timeout=1)
test_ntasks(q, Succeeded=n)

#----- creating a new task
print "-- creating a new task"
t = qdo.Task('blatfoo', qname)             #- use queue name as a string
assert isinstance(t, qdo.Task)
t = qdo.Task('blatfoo', unicode(qname))    #- use queue name as unicode
assert isinstance(t, qdo.Task)
t = qdo.Task('blatfoo', q)                 #- Queue object instead of name
assert isinstance(t, qdo.Task)

#----- These should fail to create a new task
print "-- invalid ways to create a task"
try:
    t = qdo.Task('blatfoo', qname+'invalid')
except ValueError:
    print "Correctly raised ValueError for incorrect queue name"
    t = 'Yep'
assert t == 'Yep'

try:
    t = qdo.Task('blatfoo')
except TypeError:
    print "Correctly raised TypeError for missing queue name"
    t = 'Yep'
assert t == 'Yep'

#----- running with a function
print "-- q.do with a function"
def blat(x):
    print x
    
q.rerun()
q.do(func=blat, timeout=1)

#- All of these should just run
print "-- testing print functions"
print q
print q.status()
q.print_task_state()
q.print_tasks()
q.print_tasks(state=qdo.Task.PENDING)
q.print_tasks(state=qdo.Task.PENDING, verbose=True)
qdo.print_queues()

#----- Wrap up
print """\
-------------------------------------------------------------------
Congratulations.  If you got this far every test passed.
-------------------------------------------------------------------"""
q.delete()
os.remove(cmdfile)