#!/usr/bin/env python

"""
Test qdo command line interface.
"""

import sys
import os
import qdo

def test_ntasks(q, **kwargs):
    """
    Check the number of tasks in each state.
    If a state isn't listed in kwargs, assume 0.
    """
    ntasks = q.tasks(summary=True)
    for state in ['Waiting', 'Pending', 'Running', 'Succeeded', 'Failed']:
        n = kwargs[state] if (state in kwargs) else 0            
        failmsg = "{}={} instead of {}".format(state, ntasks[state], n)
        assert ntasks[state] == n, failmsg
    

#----- Clear out any pre-existing queue
qname = 'TestBlatFoo'
q = qdo.connect(qname, create_ok=True)
q.delete()

#----- qdo create
assert 0 == os.system('qdo create {}'.format(qname))
q = qdo.connect(qname)

#----- qdo delete
assert 256 == os.system('qdo delete {}'.format(qname))
assert 0 == os.system('qdo delete {} --force'.format(qname))

try:
    q = qdo.connect(qname)
    print "Oops!  Delete didn't work!"
    sys.exit(1)
except ValueError:
    #- ok, the delete worked, so put the queue back now
    q = qdo.create(qname)

#----- qdo add
n = 3
cmdfile = qname+'.txt'
fx = open(cmdfile, 'w')
for i in range(n):
    cmd = 'qdo add {} "echo Hello {}"'.format(qname, i)
    os.system(cmd)
    print >> fx, 'echo Goodbye {}'.format(i)
fx.close()

#- Test
test_ntasks(q, Pending=n)

#----- qdo load
assert 0 == os.system('qdo load {} {}'.format(qname, cmdfile))
assert 0 == os.system('qdo load {} {} --priority 1'.format(qname, cmdfile))
assert 0 == os.system('cat {} | qdo load {} -'.format(cmdfile, qname))
test_ntasks(q, Pending=4*n)

#----- Load some tasks that should fail
for i in range(n):
    os.system('qdo add {} "oops {}"'.format(qname, i))
test_ntasks(q, Pending=5*n)

#----- qdo status
assert 0 == os.system('qdo status {}'.format(qname))

#----- qdo tasks [--state STATE] [--verbose]
assert 0 == os.system('qdo tasks {}'.format(qname))
assert 0 == os.system('qdo tasks {} --state Pending'.format(qname))
assert 0 == os.system('qdo tasks {} --verbose'.format(qname))

#----- qdo do
assert 0 == os.system('qdo do {} --timeout 1'.format(qname))
test_ntasks(q, Succeeded=4*n, Failed=n)

#----- qdo retry
assert 0 == os.system('qdo retry {}'.format(qname))
test_ntasks(q, Succeeded=4*n, Pending=n)

#----- qdo rerun
assert 256 == os.system('qdo rerun {}'.format(qname))
test_ntasks(q, Succeeded=4*n, Pending=n)
assert 0 == os.system('qdo rerun {} --force'.format(qname))
test_ntasks(q, Pending=5*n)

#----- qdo pause
assert q.state == 'Active'
assert 0 == os.system('qdo pause {}'.format(qname))
assert q.state == 'Paused'
assert q.get() is None

#----- qdo resume
assert 0 == os.system('qdo resume {}'.format(qname))
assert q.state == 'Active'
assert q.get() is not None
test_ntasks(q, Pending=5*n-1, Running=1)

#----- qdo recover
assert 0 == os.system('qdo recover {}'.format(qname))
test_ntasks(q, Pending=5*n)

#----- qdo list
assert 0 == os.system('qdo list')

#----- Wrap up
print """\
-------------------------------------------------------------------
Congratulations.  If you got this far every command line option has
been tested except "qdo launch ..."
-------------------------------------------------------------------"""
q.delete()
os.remove(cmdfile)