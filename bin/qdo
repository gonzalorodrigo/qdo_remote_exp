#!/usr/bin/env python

"""
QDO (kew-doo) is a lightweight toolkit to process many little
tasks in a queue.  This is the command line interface to the python API.

Stephen Bailey, Fall 2013
"""

import sys
import os
import subprocess
import time
import json   #- for encoding/decoding message to the queue
import uuid   #- For unique message IDs

#-------------------------------------------------------------------------

_qdohelp = """
Implemented Commands:
    qdo list                    : list all available queues
    qdo create   qname          : create qname (optional; ok to just add/load)
    qdo load     qname taskfile [--priority <float>]
                                : load tasks from file into queue
                                  if taskfile=='-' read from stdin instead
    qdo add      qname task [--priority <float>]
                                : add a single task to a queue
    qdo launch   qname njobs [--batchopts opts] [--script cmd] [--pack]
                             [--timeout t] [--walltime HH:MM:SS]
                             [--batchqueue q]
                                : launch jobs to process tasks
    qdo status   qname          : print status of queue and tasks
    qdo tasks    qname [--verbose] : print all tasks in queue
    qdo retry    qname          : move failed tasks back into pending
    qdo recover  qname          : move running tasks back into pending
    qdo rerun    qname --force  : move all tasks back into pending
    qdo pause    qname          : temporarily suspend distribution of tasks
    qdo resume   qname          : resume distributing tasks to jobs
    qdo delete   qname --force  : delete queue and db
    qdo do       qname          : run qname if marked active
    
Not yet implemented:
    qdo save   qname file       : checkpoint server queue so it can be stopped
    qdo load   qname file       : reload a previously saved queue
    
All commands except "qdo list" require a queue name to be provided.
"""

#- Do manual arg parsing because of somewhat non-standard usage.
#- Consider reorganizing how tasks are specified and parsed.
#- this is getting a little out of hand.
import optparse
parser = optparse.OptionParser(usage = "qdo command queue_name args [options]")
parser.add_option("-u", "--user",  type=str,  help="username", default=os.environ['USER'])
parser.add_option("-f", "--force", action='store_true', help="override warnings and force command")
parser.add_option("-a", "--all", action='store_true', help="list queues for all users")
parser.add_option("-v", "--verbose", action='store_true', help="verbose printing")
parser.add_option("-t", "--timeout", type=float,  help="time to wait for tasks before giving up [QDO_TIMEOUT]", default=60)
parser.add_option("--batchopts", type=str, help="options to pass to qsub")
parser.add_option("--pack", action='store_true', help="pack multiple workers into a single batch job")
parser.add_option("--walltime", dest="walltime", type=str, help="batch job walltime limit")
parser.add_option("--batchqueue", dest="batchqueue", type=str, help="batch queue name (not qdo queue name)")
parser.add_option("--state", dest='state', type=str, help="include only tasks in this state")
parser.add_option("--script", dest="script", type=str, help="script to run for each task")
parser.add_option("--priority", dest="priority", type=float, help="priority for jobs to be added")
parser.add_option("--jitter", dest="jitter", type=float, help="Startup random jitter time to avoid hitting DB too hard")

#- optparse mis-formats the extended help, so intercept help requests
if len(set(sys.argv) & set( ('-h', '-help', '--help'))) > 0 or len(sys.argv) == 1 or sys.argv[1] == 'help':
    parser.print_help()
    print _qdohelp
    sys.exit(0)
   
#- Now we can parse the options
opts, args = parser.parse_args()
action = args[0]

if opts.jitter is not None:
    import random
    time.sleep(random.uniform(0.0, opts.jitter))
    
#- Load qdo after arg parsing, in case we just want to print help without
#- spawning any backend connections or sanity checks.
import qdo
    
if action == 'list':
    if opts.all:
        qdo.print_queues()
    else:
        qdo.print_queues(opts.user)
elif len(args) == 1:
    print >> sys.stderr, 'You must provide a queue name; qdo -h for details'
    sys.exit(1)
else:
    name = args[1]
    if not qdo.valid_queue_name(name):
        sys.exit(2)

    if action == 'create':
        if name in [q.name for q in qdo.qlist(user=opts.user)]:
            print 'Queue %s already exists.' % name
            sys.exit(0)
        
    #- Some tasks require an existing queue; others it is optional
    if action in ('create', 'load', 'add'):
        q = qdo.connect(name, create_ok=True, user=opts.user)
    else:
        q = qdo.connect(name, create_ok=False, user=opts.user)
        
    if action == 'create':
        print 'Queue %s created' % name
    elif action == 'load':
        task_file = args[2]
        q.loadfile(task_file, priority=opts.priority)
    elif action == 'add':
        task = args[2]
        q.add(task, priority=opts.priority)
    elif action == 'launch':
        nworkers = int(args[2])
        q.launch(nworkers, script=opts.script,
            timeout=opts.timeout, pack=opts.pack,
            walltime=opts.walltime, batchqueue=opts.batchqueue,
            batch_opts=opts.batchopts)
    elif action == 'status':
        q.print_task_state()
    elif action == 'retry':
        n = len(q.retry())
        if n > 0:
            print "%d tasks reset to pending" % n
        else:
            print "no failed tasks found to retry"
    elif action == 'recover':
        n = len(q.recover())
        if n > 0:
            print "%d tasks reset to pending" % n
        else:
            print "no running tasks found to retry"
    elif action == 'rerun':
        if opts.force:
            n = len(q.rerun())
            if n > 0:
                print "%d tasks reset to pending" % n
            else:
                print "no tasks found to rerun"
        else:
            print "ERROR: To prevent accidental resets, you must use --force with rerun"
            print "No action taken"
            sys.exit(1)
    elif action == 'tasks':
        q.print_tasks(opts.verbose, state=opts.state)
    elif action == 'pause':
        q.pause()
        print "Queue %s paused; use 'qdo resume %s' to resume" % (name, name)
    elif action == 'resume':
        q.resume()
        print "Queue %s resumed" % name
    elif action == 'delete':
        if opts.force:
            q.delete()
        else:
            print >> sys.stderr, "To prevent accidental deletions, you must use --force with delete"
            sys.exit(1)
    elif action == 'do':
        if q.state == qdo.Queue.ACTIVE:
            q.do(timeout=opts.timeout, script=opts.script)
        else:
            print "Queue %s is paused; not processing commands" % name
            print "Use 'qdo resume %s' to resume" % name
    else:
        print 'ERROR: Command "%s" not recognized' % action
        sys.exit(1)
        
