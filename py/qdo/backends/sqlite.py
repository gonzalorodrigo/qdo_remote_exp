"""
QDO backend using an sqlite database.

Stephen Bailey, early 2014
"""

import sys
import os
import subprocess
import time
from glob import glob
import sqlite3
import uuid
import json
import random

from qdo import Queue, Task
import qdo

#-------------------------------------------------------------------------
#- Helper functions

def _basedir():
    """
    return $QDO_DB_DIR or otherwise $HOME/.qdo/db
    """
    if 'QDO_DB_DIR' in os.environ:
        return os.getenv('QDO_DB_DIR')
    else:
        return os.getenv('HOME')+'/.qdo/db'

def _dbdir(queuename, user=None, basedir=None):
    """
    Return directory where sqlite db file is stored
    filling in defaults for user=$USER and basedir=_basedir() if needed.
    
    dbdir = /{basedir}/{username}/{queuename}/queue.sqlite
    """
    
    if user is None:
        user = os.getenv('USER')
        
    if basedir is None:
        basedir = _basedir()

    return os.path.join(basedir, user, queuename)
    
def _dbfile(queuename, user=None, basedir=None):
    return os.path.join(_dbdir(queuename, user, basedir), 'queue.sqlite')
    
def _queue_exists(queuename, user=None, basedir=None):
    qfile = _dbfile(queuename, user=user, basedir=basedir)
    return os.path.exists(qfile)

#-------------------------------------------------------------------------
#- Sqlite specific Backend subclass

class SqliteBackend(qdo.Backend):
    """sqlite3 based backend object"""
    def __init__(self, basedir=None):
        """
        Create a new SqliteBackend object
        
        If basedir is None, use $QDO_DB_DIR or otherwise $HOME/.qdo/db/
        """
        if basedir is None:
            self._basedir = _basedir()
        else:
            self._basedir = basedir

    def create(self, queuename, user=None, worker=None):
        """
        Create a new queue and return a subclass of qdo.Queue
        
        Raises ValueError if queue_name already exists
        
        Optional inputs:
            user    : name of user who owns this queue (default $USER)
            worker  : name of worker connecting to this queue
        """
        dbfile = _dbfile(queuename, user, self._basedir)
        if not os.path.exists(dbfile):
            return SqliteQueue(queuename, user=user, worker=worker,
                    basedir=self._basedir, create=True)
        else:
            raise ValueError('Queue {} already exists'.format(queuename))

    def connect(self, queuename, user=None, worker=None, create_ok=False):
        """
        Return a subclass of qdo.Queue representing queuename
        
        Optional inputs:
            user      : name of user who owns this queue (default $USER)
            worker    : name of worker connecting to this queue
            create_ok : if True, create queue if needed.  If False (default),
                        raise ValueError if queue doesn't already exist.
        """
        dbfile = _dbfile(queuename, user, self._basedir)

        create = False
        if (not os.path.exists(dbfile)) and create_ok:
            create = True

        return SqliteQueue(queuename, user=user, worker=worker,
                basedir=self._basedir, create=create)

    def qlist(self, user=None):
        """Return a list of qdo.Queue objects for known queues"""
        #- sqlite queues in /base_dbdir/{USERNAME}/{QUEUENAME}/queue.sqlite

        queues = list()
        if user is None:
            queuefiles = glob(self._basedir+ '/*/*/queue.sqlite')
        else:
            queuefiles = glob(os.path.join(self._basedir, user, '*/queue.sqlite'))
        
        for filename in sorted(queuefiles):
            qname = filename.split('/')[-2]
            queues.append(SqliteQueue(qname, user=user))

        return queues

class SqliteQueue(qdo.Queue):
    def __init__(self, name, user=None, worker=None, basedir=None, create=False):
        self.name = name
        if user is None:
            user = os.environ['USER']
        self._user = user

        if worker is None:
            self._worker = qdo._worker()
        else:
            self._worker = worker
            
        if basedir is None:
            basedir = _basedir()
            
        self._dbfile = _dbfile(name, user=user, basedir=basedir)
        self._dbdir = os.path.dirname(self._dbfile)
        
        #- Check if it already exists and/or if we are supposed to create it
        if create and os.path.exists(self._dbfile):
            raise ValueError('Queue {} already exists'.format(name))
        elif (not os.path.exists(self._dbfile)) and (not create):
            raise ValueError('Queue {} does not exist'.format(name))
            
        #- Create directory with rwx for user but no one else
        if not os.path.exists(self._dbdir):
            os.makedirs(self._dbdir, mode=0700)
        
        if not create:
            self.db = sqlite3.Connection(self._dbfile, timeout=60)
        else:
            self.db = sqlite3.Connection(self._dbfile)
            
            #- Give rw access to user but no one else
            os.chmod(self._dbfile, 0600)

            #- Create tables
            self.db.executescript("""\
            CREATE TABLE tasks (
              id       TEXT PRIMARY KEY,
              task     TEXT,
              state    TEXT,
              priority REAL
            );
            
            --  index on tasks.priority for faster get queries
            CREATE INDEX task_priority on tasks(priority);

            -- Dependencies table.  Multiple entries for multiple deps.
            CREATE TABLE dependencies (
              task_id  TEXT,     -- task.id foreign key
              requires TEXT,     -- task.id that it depends upon
              -- Add foreign key constraints
              FOREIGN KEY(task_id) REFERENCES tasks(id),
              FOREIGN KEY(requires) REFERENCES tasks(id)
            );       

            -- Log table of state transitions
            CREATE TABLE log (
                task_id TEXT,
                state   TEXT,
                worker  TEXT default "unknown",
                time    DATETIME DEFAULT CURRENT_TIMESTAMP,
                err     INTEGER default 0,
                message TEXT,
                FOREIGN KEY(task_id) REFERENCES tasks(id)
            );

            -- Metadata about this queue, e.g. active/paused
            CREATE TABLE metadata (
                key   TEXT,
                value TEXT
            );
            """)

            #- Initial queue state is active
            self.db.execute('INSERT INTO metadata VALUES (?,?)', ('queue_state', Queue.ACTIVE))

            self.db.commit()

    @property
    def user(self):
        return self._user;
    
    #- Get and release locks on the DB
    def _getlock(self, timeout=10.0):
        wait_time = 1.0
        while True:
            try:
                self.db.execute('BEGIN IMMEDIATE')
                return True
            except sqlite3.OperationalError:
                if wait_time > timeout:
                    print "ERROR: unable to get database lock"
                    sys.stdout.flush()
                    return False
                else:
                    print "WARNING: unable to get db lock; will retry in {} sec".format(wait_time)
                    sys.stdout.flush()
                    time.sleep(wait_time)
                    wait_time *= 2.0 * random.uniform(0.8, 1.2)
           
        
    def _releaselock(self):
        self.db.commit()
        
    #- sql wrappers
    def _dbx(self, query, args=None, timeout=120.0, multiple=False):
        """
        Perform a database query retrying if needed.  If timeout seconds
        pass, then re-raise sqlite3.OperationalError from locked db.
        
        If multiple=True, calls db.executemany(query, args) instead of
        db.execute(query).
        
        Returns result of query.
        """
        wait_time = 1.0
        ntries = 1
        while True:
            try:
                if ntries > 1:
                    print "INFO: retrying"
                    print query
                    print " ", args

                if multiple:
                    result = self.db.executemany(query, args)
                elif args is None:
                    result = self.db.execute(query)
                else:
                    result = self.db.execute(query, args)
                    
                if ntries > 1:
                    print "INFO: succeeded after {} tries".format(ntries)
                    
                return result
                
            except (sqlite3.OperationalError, sqlite3.DatabaseError) as err:
                #- A few known errors that can occur when multiple clients
                #- are hammering on the database.  For these cases, wait
                #- and try again a few times before giving up.
                known_errors = [
                    'database is locked',
                    'database disk image is malformed', #- on NFS
                ]
                if err.message in known_errors:
                    if wait_time < timeout:
                        print "INFO: {} on attempt {}.  Will retry in {:.2f} sec".format(err.message, ntries, wait_time)
                        self.db.close()
                        time.sleep(wait_time)
                        print "INFO: reconnecting..."
                        self.db = sqlite3.Connection(self._dbfile)
                        wait_time *= 2.0 * random.uniform(0.8,1.2)
                        ntries += 1
                    else:
                        print "ERROR: tried {} times and still getting errors".format(ntries)
                        raise err
                else:
                    raise err
    
    def _log(self, id, state, err=0, message=None):
        """
        Add log entry but do *not* commit
        """
        update = 'INSERT INTO log(task_id,state,worker,err,message) VALUES (?,?,?,?,?)'
        self._dbx(update, (id, state, self._worker, err, message))
    
    #--------------------------------------------------------------------
    def _add(self, task, id=None, priority=0.0, requires=None):
        if id is None:
            id = uuid.uuid4().hex
            
        if requires is None:
            state = Task.PENDING
        else:
            state = Task.WAITING
            
        task = json.dumps(task)
        q = 'INSERT INTO tasks (id, task, state, priority) VALUES (?,?,?,?)'
        self._dbx(q, (id, task, state, priority))
        self._log(id, state)
        
        #- Add dependencies, but then also check if they are already done
        if requires is not None:
            self._add_dependency(id, requires)
            self._update_waiting_task_state(id)
        
        self.db.commit()
        
        return id

    def _add_dependency(self, taskid, requires):
        """
        Add dependencies for a task
        
        taskid : string task id
        requires : list of ids upon which taskid depends
        """
        query = 'INSERT INTO dependencies (task_id, requires) VALUES (?, ?)'
        if isinstance(requires, str):
            self._dbx(query, (taskid, requires))
        else:
            args = [(taskid, x) for x in requires]
            self._dbx(query, args, multiple=True)
        
        self.db.commit()  #- When should I commit?
        
    
    #- Use bulk inserts to add multiple items more efficiently
    def _add_multiple(self, tasks, ids=None, priorities=None, requires=None):
        #- Convert tasks to JSON
        tasks = [json.dumps(x) for x in tasks]
        if ids is None:
            ids = [uuid.uuid4().hex for x in tasks]
        if priorities is None:
            priorities = [0.0,] * len(tasks)

        #- If no dependencies, directly insert into Pending state;
        #- otherwise insert into Waiting state
        initial_state = Task.PENDING if (requires is None) else Task.WAITING
            
        q = 'INSERT INTO tasks (id, task, state, priority) VALUES (?,?,"%s",?)' % initial_state
        self._dbx(q, zip(ids, tasks, priorities), multiple=True)
        q = 'INSERT INTO log(task_id,state,worker) VALUES (?,"%s","%s")' % (initial_state, self._worker)
        self._dbx(q, zip(ids), multiple=True)
        self.db.commit()
        
        #- Add dependencies if needed
        if requires is not None:
            for taskid, depids in zip(ids, requires):
                if depids is not None:
                    ### print taskid, "depends upon", depids
                    self._add_dependency(taskid, depids)
                    self._update_waiting_task_state(taskid)
                    
        return ids

    
    #- functions to update states based on dependencies
    def _update_waiting_task_state(self, taskid, force=False):
        """
        Check if all dependencies of this task have finished running.
        If so, set it into the pending state.
        
        if force, do the check no matter what.  Otherwise, only proceed
        with check if the task is still in the Waiting state.
        """
        if not self._getlock():
            print "ERROR: unable to get db lock; not updating waiting task state"

        #- Ensure that it is still waiting
        #- (another process could have moved it into pending)
        if not force:
            q = 'SELECT state FROM tasks where tasks.id = ?'
            try:
                state = self.db.execute(q, (taskid,)).next()[0]
            except StopIteration:
                self._releaselock()
                raise ValueError("TaskID %s not found" % str(taskid))
            if state != Task.WAITING:
                self._releaselock()
                return
        
        #- Count number of dependencies that are still pending or waiting
        count_unfinished = """\
        SELECT COUNT(d.requires)
        FROM dependencies d JOIN tasks t ON d.requires = t.id
        WHERE d.task_id = ? AND t.state IN ("%s", "%s", "%s")
        """ % (Task.PENDING, Task.WAITING, Task.RUNNING)
        try:
            n = self.db.execute(count_unfinished, (taskid,)).next()[0]
        except StopIteration:
            self._releaselock()
            return

        if n == 0:
            self.set_task_state(taskid, Task.PENDING)
        elif force:
            self.set_task_state(taskid, Task.WAITING)

        self._releaselock()
    
    def _update_waiting_tasks(self, taskid):
        """
        Identify any tasks that are waiting for taskid, and call
        _update_waiting_task_state() on them.
        """
        q = """\
        SELECT id FROM tasks t JOIN dependencies d ON d.task_id = t.id
        WHERE d.requires = ? AND t.state = "%s"
        """ % Task.WAITING
        waiting_tasks = self._dbx(q, (taskid,)).fetchall()
        for taskid, in waiting_tasks:
            self._update_waiting_task_state(taskid)
    
    #- Get / Set state of the queue
    @property
    def state(self):
        state = self._dbx('SELECT value FROM metadata WHERE key="queue_state"').next()[0]
        return state
        
    @state.setter
    def state(self, state):
        if state not in (Queue.ACTIVE, Queue.PAUSED):
            raise ValueError("Invalid queue state %s; should be %s or %s" % (state, Queue.ACTIVE, Queue.PAUSED))
            
        self._dbx('UPDATE metadata SET value=? where key="queue_state"', (state,))
        self.db.commit()
        
    #- Get a task
    def _get(self, timeout):
        #- First make sure we aren't paused
        if self.state == Queue.PAUSED:
            return None
        
        t0 = time.time()
        while True:
            if self._getlock():
                try:
                    getone = """\
                        SELECT id, task FROM tasks 
                        WHERE state="%s"
                        ORDER BY priority DESC, rowid
                        LIMIT 1""" % Task.PENDING
                    taskid, task = self._dbx(getone).next()
                    break
                except StopIteration:
                    self._releaselock()
                    if time.time() - t0 > timeout:
                        ### print "No tasks in queue; giving up"
                        return None
                    else:
                        twait = min(timeout, 5)
                        print "No tasks in %s; waiting %d seconds" % (self.name, twait)
                        time.sleep(twait)
            else:
                print "There may be tasks left in queue but I couldn't get lock to see"
                return None

        self._set_task_state(taskid, Task.RUNNING)
        self._releaselock()
         
        return qdo.Task(json.loads(task), queue=self, id=taskid, state=Task.RUNNING)
         
    def _set_task_state(self, taskid, state, err=0, message=None):
        self._getlock()
        try:
            update = "UPDATE tasks SET state=? WHERE id=?"
            self._dbx(update, (state, taskid))        
            self.db.commit()
            self._log(taskid, state, err=err, message=message)
            ### self.db.commit()
        
            if state in (Task.SUCCEEDED, Task.FAILED):
                self._update_waiting_tasks(taskid)

        except Exception, e:
            self._releaselock()
            raise e
        
        self.db.commit()
        self._releaselock()

    def _retry(self, state=Task.FAILED):
        if not self._getlock():
            print "ERROR: unable to get lock; not retrying any tasks"
            return list()
        
        cursor = self.db.execute('SELECT id FROM tasks WHERE state=?', (state,))
        taskids = [x[0] for x in cursor.fetchall()]

        #- In a lock, so set of tasks in that state should still be the same
        if len(taskids) > 0:
            self.db.execute('UPDATE tasks SET state=? WHERE state=?', (Task.WAITING, state))
            log = 'INSERT INTO log(task_id,state,worker) VALUES (?,"{}","{}")'.format(Task.WAITING, self._worker)
            self._dbx(log, zip(taskids), multiple=True)

            #- Check if any Pending tasks should now move back to Waiting
            cursor = self.db.execute('SELECT id FROM tasks WHERE state=?', (Task.PENDING,))
            pending_taskids = [x[0] for x in cursor.fetchall()]
            for id in pending_taskids:
                ### print "Checking", id
                self._update_waiting_task_state(id, force=True)
                
        self._releaselock()
        
        #- See if any of these can move from Waiting -> Pending
        for id in taskids:
            self._update_waiting_task_state(id)

        return taskids
        
    def _rerun(self):
        if not self._getlock():
            print "ERROR: unable to get lock; not rerunning any tasks"
            return list()
            
        failed = self._retry(Task.FAILED)
        succeeded = self._retry(Task.SUCCEEDED)
        self._releaselock()
        return failed+succeeded
     
    def _pause(self):
        self.state = Queue.PAUSED
     
    def _resume(self):
        self.state = Queue.ACTIVE
     
    def _delete(self):
        self.db.close()
        del self.db
        import shutil
        #- ignore_errors = True is needed on NFS systems; this might
        #- leave a dangling directory, but the sqlite db file itself
        #- should be gone so it won't appear with qdo list.
        shutil.rmtree(self._dbdir, ignore_errors=True)

    def _tasks(self, id=None, state=None, summary=False):
        """
        Return list of Task objects for this queue.

        if state != None, return only tasks in that state
            (see Tasks.VALID_STATES)

        if id != None, return just that Task object.

        if summary=True, return dictionary of how many tasks are in each state.
        """
        #- If summary, just return count of tasks in each state
        if summary:
            ntasks = dict()
            count = "SELECT count(state) FROM tasks WHERE state=?"
            for state in Task.VALID_STATES:
                ntasks[state] = self._dbx(count, (state,)).next()[0]
            return ntasks

        results = list()
        query = """\
        SELECT tasks.id,task,tasks.state,err,message
        FROM tasks JOIN log ON
            (tasks.id = log.task_id AND tasks.state = log.state)
        """
        if state is not None:
            query += ' AND tasks.state="{}"'.format(state)
            
        if id is not None:
            query += ' AND tasks.id="{}"'.format(id)
            
        query += ' GROUP BY log.task_id ORDER BY time'
        
        #- If id is set, get just that task
        if id is not None:
            try:
                id,jsontask,taskstate,err,message = self._dbx(query).next()
            except StopIteration:
                raise ValueError, "Task ID {} doesn't exist".format(id)
                
            task = Task(json.loads(jsontask), self, id=id, state=taskstate,
                        err=err, message=message)
            return task
            
        #- Otherwise return them all
        else:
            for id,jsontask,taskstate,err,message in self._dbx(query).fetchall():
                task = Task(json.loads(jsontask), self, id=id, state=taskstate,
                            err=err, message=message)
            
                results.append(task)
            
            return results
        
    def history(self, taskid):
        q = """\
            SELECT task_id,state,worker,time,err,message
            FROM log WHERE task_id=?
            ORDER BY time"""
        results = list()
        for id,state,worker,time,err,message in self._dbx(q, (taskid,)).fetchall():
            results.append(dict(id=id,state=state,worker=worker,time=time,err=err,message=message))
        return results
        
        
    


