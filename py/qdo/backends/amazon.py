#!/usr/bin/env python

"""
DEPRECATED / BROKEN.  Do not use.

Backend QDO Queue using Amazon Web Services.

Tasks are managed in a SimpleDB plus a Simple Queue Service to handle
the race condition of multiple jobs asking for a new Task.  The downside
is that the two could get out-of-sync if a transaction is interupted.

Stephen Bailey, Fall 2013
"""

import sys
import os
import time

from qdo import Queue, Task
import qdo

QDO_AWS_REGION = 'us-west-1'

try:
    import boto
    import boto.sqs
    import boto.sdb
except ImportError:
    print >> sys.stderr, "ERROR: boto is required for the QDO Amazon backend."
    print >> sys.stderr, "you can get it at https://github.com/boto/boto"
    print >> sys.stderr, "Then create a $HOME/.boto file with your AWS credentials."
    print >> sys.stderr, "http://docs.pythonboto.org/en/latest/getting_started.html"
    sys.exit(1)

class AmazonQueue(qdo.Queue):
    
    def _fullname(self, name):
        """
        Make standardized full queue name including user name so different
        users with the same AWS credentials don't step on each other.
        """
        return '%s__%s' % (self.opts.user, name)
    
    def __init__(self, name, user=None, existing_queue=True):
        """
        Create a QDO object with a connection to Amazon Web Services
        Queue with Tasks, and SimpleDB with status of those tasks.
        
        user : user name, default os.environ['USER']
        existing_queue : if True, require that queue exists
        """
        if not qdo.valid_queue_name(name):
            raise ValueError, name+" is not a valid queue name [alphanumeric+underscores]"
        
        if user is None:
            self.user = os.environ['USER']
        else:
            self.user = user

        self.name = name
        self.fullname = self.user + '__' + self.name
            
        #- Connect to AWS
        sqs = boto.sqs.connect_to_region(QDO_AWS_REGION)
        sdb = boto.sdb.connect_to_region(QDO_AWS_REGION)
        
        #- Check if queue already exists
        if existing_queue:
            if self.fullname not in [q.name for q in sqs.get_all_queues()]:
                err = "ERROR: Queue %s for user %s doesn't exist" % (self.name, self.user)
                raise ValueError, err
        
        self._q = sqs.create_queue(self.fullname, visibility_timeout=3600)
        self._db = sdb.create_domain(self.fullname)

        #- If creating a new queue, make it active
        if self._db.get_item('meta') is None:
            self._db.put_attributes('meta', dict(queue_status=Queue.ACTIVE))        
    
    def _add(self, task, id=None):
        """
        Add a single task to the queue
        """
        task = qdo.Task(task=task, id=id, queue=self)
        
        msg = self._q.new_message(task.encode())
        self._q.write(msg)
        task.set_status(Task.PENDING, task=task.task)
        
        return task

    def _update_task(self, id, **kwargs):
        """
        Update the task DB with keyword/value pairs associated with the
        task id.
        
        Examples:
        q.update_task(task.id, status=Task.RUNNING, start_time=time.time())
        ...
        q.update_task(task.id, status=Task.SUCCEEDED, end_time=time.time())
        """
        # x = dict()
        # for key, value in kwargs.items():
        #     x[key] = value
        #     
        # self._db.put_attributes(id, x)
        
        self._db.put_attributes(id, kwargs)

    def _get(self, timeout=60, retrytime=5):
        """
        Get a task from the queue

        If no tasks are available, retry every `retrytime` seconds,
        untile `timeout` seconds have passed, then return None.
        """
        wait_time = 0
        while True:
            queue_status = self._db.get_item('meta')['queue_status']
            if queue_status != Queue.ACTIVE:
                print "Queue status %s; stopping" % queue_status
                return None
            
            m = self._q.read()
            if m is not None:
                self._q.delete_message(m)
                task = qdo.Task.decode(m.get_body(), queue=self)
                task.set_status(Task.RUNNING)
                return task
            else:
                if wait_time >= timeout:
                    print "No tasks for %d seconds; stopping" % wait_time
                    return None
                else:
                    print 'No tasks; waiting %d seconds' % retrytime
                    wait_time += retrytime
                    time.sleep(retrytime)
                    continue

    def _retry(self, state):
        """
        Move tasks in `state` (e.g. Task.FAILED) back into pending

        Returns number of jobs moved back to pending
        
        WARNING: this could leave queue/db in an inconsistent state if
        it fails in the middle.
        """
        n = 0
        query = 'select * from %s where status="%s"' % (self.fullname, state)
        for result in self._db.select(query, consistent_read=True):
            task = qdo.Task(task=result['task'], id=result.name, queue=self)      
            self._q.write(self._q.new_message(task.encode()))
            task.set_status(Task.PENDING)
            n += 1

        return n

    def _rerun(self):
        """
        Move all non-pending tasks back into pending
        
        Returns number of jobs moved back to pending
        
        WARNING: this could leave queue/db in an inconsistent state if
        it fails in the middle.
        """
        n = 0
        query = 'select * from %s' % self.fullname
        for result in self._db.select(query, consistent_read=True):
            if 'status' not in result: continue  #- skip metadata entry
            if result['status'] != Task.PENDING:
                task = qdo.Task(task=result['task'], id=result.name, queue=self)        
                self._q.write(self._q.new_message(task.encode()))
                task.set_status(Task.PENDING)
                n += 1
        return n

    def _count_task_status(self):
        """
        Count how many jobs are pending/running/succeeded/failed.
        
        Returns dictionary of results[status] = count
        """
        consistent_read = False   #- Faster, but is this really what we want?
        result = dict()
        for status in (Task.PENDING, Task.RUNNING, Task.SUCCEEDED, Task.FAILED):
            query = 'select count(*) from %s where status="%s"' % (self.fullname, status)
            n = self._db.select(query, consistent_read=consistent_read).next()['Count']
            result[status] = int(n)

        return result

    #- TODO: rethink error vs. Exception
    def _delete(self, force=False):
        """
        Delete the AWS Queue and SimpleDB.  Use force=True to delete even if
        there are pending jobs.
        """
        if not force:
            count = self.count_task_status(consistent_read=True)
            if count[Task.PENDING] > 0:
                print 'ERROR: %d tasks still pending; use --force to delete' % count[Task.PENDING]
                return
            if count[Task.RUNNING] > 0:
                print 'ERROR: %d tasks still running; use --force to delete' % count[Task.RUNNING]
                return
                
            if self.user != os.environ['USER']:
                print "WARNING: queue owner (%s) may not be you (%s)" % (self.user, os.environ['USER'])
                print "         use --force to delete (and be careful...)"
                return
            
        self._db.delete()
        self._q.delete()

    def _status(self):
        """
        Return string describing queue status: Queue.ACTIVE or Queue.PAUSED
        """
        try:
            queue_status = self._db.get_item('meta')['queue_status']
        except:
            queue_status = Queue.UNKNOWN
        return queue_status

    def _pause(self):
        """
        Pause queue to prevent further processing
        
        This allows running tasks to cleanly finish but new tasks
        won't be processed until resume() is called and jobs are resubmitted.
        """
        self._db.put_attributes('meta', dict(queue_status=Queue.PAUSED))

    def _resume(self):
        """
        Restart a paused queue
        """
        self._db.put_attributes('meta', dict(queue_status=Queue.ACTIVE))
        
    def _task_list(self):
        query = 'select status, id, err, task from %s' % self.fullname
        results = list()
        for x in self._db.select(query):
            if 'status' in x and 'task' in x:
                tmp = dict(id=x.name)
                if 'err' in x:
                    tmp['err'] = x['err']
                tmp['status'] = x['status']
                tmp['task'] = x['task']
                results.append(tmp)

        return results
        
        
BackendQueue = AmazonQueue

def get_queues(user=None):
    """Return list of known Queue objects"""

    #- Set of known SQS queues with entries (user, qname)
    known_queues = set()
    sqs = boto.sqs.connect_to_region(QDO_AWS_REGION)
    for q in sqs.get_all_queues():
        if q.name.count('__') == 0: continue
        queue_user, _, queue_name = q.name.partition('__')
        if user is None or queue_user == user:
            known_queues.add((queue_user, queue_name))

    #- Known SDB databases, keyed by (user, qname) with value = status of queue
    known_databases = dict()
    sdb = boto.sdb.connect_to_region(QDO_AWS_REGION)
    for db in sdb.get_all_domains():
        if db.name.count('__') == 0: continue
        db_user, _, db_name = db.name.partition('__')
        if user is None or db_user == user:        
            key = (db_user, db_name)
            try:
                known_databases[key] = db.get_item('meta')['queue_status']
            except:
                known_databases[key] = Queue.UNKNOWN
        
    qdo_queues = known_queues & set(known_databases.keys())
    result = list()
    for quser, qname in sorted(qdo_queues):
        result.append(AmazonQueue(qname, user=quser, existing_queue=True))
        
    return result
        
def _check_boto_config():
    """
    Check if $HOME/.boto exists
    """
    dotboto = os.environ['HOME'] + '/.boto'
    if not os.path.exists(dotboto):
        print "ERROR: missing $HOME/.boto configuration file."
        print "Create one with your Amazon Web Services credentials:"
        print
        print "[Credentials]"
        print "aws_access_key_id = YOUR_AWS_ACCESS_KEY"
        print "aws_secret_access_key = YOUR_AWS_SECRET_KEY"
        print
        print "Also see http://boto.readthedocs.org/en/latest/getting_started.html"
        raise Exception, "missing $HOME/.boto config file"

#- Check boto config upon import, but allow pydoc to still work
#- Must also catch ipython case since it imports pydoc automatically
if 'IPython' in sys.modules or 'pydoc' not in sys.modules:
    _check_boto_config()
