## Dependencies ##

The qdo python interface includes the ability to set task dependencies,
requiring that certain tasks finish before others can begin.  e.g.

    q = qdo.create('EchoChamber')
    task1 = q.add('echo 1')
    task2 = q.add('echo 2')
    task3 = q.add('echo 3', requires=[task1, task2])
    task4 = q.add('echo 4', requires=[task3,])
    q.do(timeout=1, quiet=True)

Results:

    2
    1
    3
    4
    
Dependencies trump priorities.  In the next example, the later
tasks have higher priority but depend upon earlier tasks, so they won't
start until their dependencies have finished.

    q = qdo.create('EchoChamber2')
    task1 = q.add('echo 1', priority=1)
    task2 = q.add('echo 2', priority=0)
    task3 = q.add('echo 3', requires=(task1, task2), priority=10)
    task4 = q.add('echo 4', requires=(task3,), priority=100)
    q.do(timeout=1, quiet=True)

Results:

    1
    2
    3
    4

Task 1 (priority 1) ran before task 2 (priority 0), but both ran before
task 3 (priority 10) and task 4 (priority 100) because dependencies
trump priorities.

Dependencies can also be specified using lists-of-lists (or tuples) passed
to add_multiple:

    q = qdo.create('EchoChamber3')
    n = 3
    commandsA = ['echo A'+str(i) for i in range(n)]
    taskidsA = q.add_multiple(commandsA)

    commandsB = ['echo B'+str(i) for i in range(n)]
    commandsC = ['echo C'+str(i) for i in range(n)]

    #- task Bi and Ci will wait for Ai to finish
    taskidsB = q.add_multiple(commandsB, requires=taskidsA)
    taskidsC = q.add_multiple(commandsC, requires=taskidsA)

    #- Tasks can have multiple dependencies: Di will wait for both Bi and Ci
    commandsD = ['echo D'+str(i) for i in range(n)]
    dependenciesD = zip(taskidsB, taskidsC)
    taskidsD = q.add_multiple(commandsD, requires=dependenciesD)

    q.do(timeout=1, quiet=True)

Results:

    A2
    C2
    B2
    D2
    A1
    C1
    B1
    D1
    A0
    C0
    B0
    D0

Note that the first task processed could have been A0, A1 or A2.
Once A2 was selected and run, B2 and C2 became eligible to run along with
A1 and A2.  C2 was selected and then B2, making D2 eligible.  etc.
At any point A1 or A2 could have been selected instead by they were not
until after the initial A2 -> B2, C2 -> D2 chain was finished.

To ensure that *all* of one level of tasks is finished before the next
level begins, either make every task of each level depend upon all tasks
of the previous level, or combine dependencies with priorities, e.g.

    q = qdo.create('EchoChamber4')
    n = 3
    commandsA = ['echo A'+str(i) for i in range(n)]
    prioA = [10,] * n
    taskidsA = q.add_multiple(commandsA, priorities=prioA)

    commandsB = ['echo B'+str(i) for i in range(n)]
    commandsC = ['echo C'+str(i) for i in range(n)]
    prioBC = [5,] * n

    #- task Bi and Ci will wait for Ai to finish
    taskidsB = q.add_multiple(commandsB, requires=taskidsA, priorities=prioBC)
    taskidsC = q.add_multiple(commandsC, requires=taskidsA, priorities=prioBC)

    #- Tasks can have multiple dependencies: Di will wait for both Bi and Ci
    commandsD = ['echo D'+str(i) for i in range(n)]
    dependenciesD = zip(taskidsB, taskidsC)
    prioD = [1,] * n
    taskidsD = q.add_multiple(commandsD, requires=dependenciesD, priorities=prioD)

    q.do(timeout=1, quiet=True)

Results:

    A2
    A1
    A0
    C2
    C1
    C0
    B2
    B1
    B0
    D2
    D1
    D0
    
Note that all the Ai tasks ran first this time because they had higher
priority.  Similarly, after B2 finished D2 became eligible to run, but
it waited for the rest of the Bi and Ci to finish because they had
higher priority.

### Dependencies alone vs. dependencies plus priorities ###

In the case of a single worker, it doesn't matter if you create task
execution order with dependencies or priorities.  But if multiple workers
are polling the queue for tasks, there is a difference:

  * priorities control the order in which eligible tasks *start*
  * dependent tasks wait for their required tasks to *finish* before starting

Example:
  
    import random
    q = qdo.create('EchoChamber5')
    n = 10
    tasksA = list()
    for i in range(n):
        cmd = 'echo A%d; sleep %f' % (i, random.uniform(0,5))
        tasksA.append( q.add(cmd, priority=10) )

    q.add('echo B')
    q.add('echo C', requires=zip(tasksA))

B and C will both wait for all As to start.  But if multiple workers are
polling for tasks, B could be selected to run even while some As are
still finishing as long as there is a slot available.  C, on the other hand,
will wait for all the As to *finish*, and then it will start.

### Finish or succeed? ###

Tasks wait for their depenciencies to *finish*, but don't require
that they succeed.  Failure is still finishing.  Future versions of qdo
will also support a type of dependency that requires the tasks to succeed.
If any dependency fails, the waiting task will be blocked until the
problem is resolved and the failed task is rerun and successfully completes.